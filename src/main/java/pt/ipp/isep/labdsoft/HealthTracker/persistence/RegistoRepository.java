package pt.ipp.isep.labdsoft.HealthTracker.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.HealthTracker.domain.Registo;
import pt.ipp.isep.labdsoft.HealthTracker.domain.Tracker;


import java.util.List;


@Repository
public interface RegistoRepository extends JpaRepository <Registo, Long> {
    public List<Registo> findRegistoByTracker(Tracker tracker);
}
