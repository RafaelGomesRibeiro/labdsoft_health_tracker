package pt.ipp.isep.labdsoft.HealthTracker.services.rabbitmq.dispatcher;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public final class TendenciasIdentificadasEvent implements Serializable {
    private List<String> tendenciasIdentificadas;
}