package pt.ipp.isep.labdsoft.HealthTracker.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class RegistoDTO {
    public TrackerDTO tracker;
    public Double tensao;
    public Double temperatura;
    public Double freqCardiaca;
    public String dataRegisto;
}
