package pt.ipp.isep.labdsoft.HealthTracker.services;

import pt.ipp.isep.labdsoft.HealthTracker.dto.EditTrackerDTO;
import pt.ipp.isep.labdsoft.HealthTracker.dto.TrackerDTO;

import java.util.List;


public interface TrackerService {

    List<TrackerDTO> listAll();
    TrackerDTO byId(Integer id);
    TrackerDTO byNumPaciente(Integer numPaciente);
    TrackerDTO createTracker(TrackerDTO dto);
    TrackerDTO updateTracker(EditTrackerDTO dto, Integer id);
    TrackerDTO associarTracker(Integer numTracker, Integer numPac);
    List<Long> utentesSemTracker();
    List<Long> utentesComTracker();

}
