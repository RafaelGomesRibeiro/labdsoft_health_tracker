package pt.ipp.isep.labdsoft.HealthTracker.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import pt.ipp.isep.labdsoft.HealthTracker.domain.Tracker;
import pt.ipp.isep.labdsoft.HealthTracker.dto.EditTrackerDTO;
import pt.ipp.isep.labdsoft.HealthTracker.dto.TrackerDTO;
import pt.ipp.isep.labdsoft.HealthTracker.dto.UtenteDTO;
import pt.ipp.isep.labdsoft.HealthTracker.persistence.TrackerRepository;


import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class TrackerServiceImpl implements TrackerService {

    private final double DEFAULT_VALUE_INF = 10.0;
    private final double DEFAULT_VALUE_SUP = 90.0;


    @Value("${UTENTE_BASE_URL}")
    private String usersBaseURL;

    private final WebClient webClient = WebClient.create();


    @Autowired
    private TrackerRepository repo;
    @Autowired
    private ValidaAtribuicaoService validaService;

    @Override
    public List<TrackerDTO> listAll() {
        return repo.findAll().stream().map(Tracker::toDTO).collect(Collectors.toList());
    }

    @Override
    public TrackerDTO byId(Integer id) {
        Tracker tracker = repo.findTrackerByTrackerId(id).orElseThrow(() -> new NoSuchElementException("Tracker com id " + id + " não existe"));
        return tracker.toDTO();
    }

    @Override
    public TrackerDTO byNumPaciente(Integer numPaciente) {
        Tracker tracker = repo.findTrackerByNumPaciente(numPaciente).orElseThrow(() -> new NoSuchElementException("Tracker do paciente " + numPaciente + " não existe"));
        return tracker.toDTO();
    }

    @Override
    public TrackerDTO createTracker(TrackerDTO dto) {
        Tracker tracker = repo.save(new Tracker(null, dto.trackerId, DEFAULT_VALUE_INF, DEFAULT_VALUE_SUP, DEFAULT_VALUE_INF, DEFAULT_VALUE_SUP, DEFAULT_VALUE_INF, DEFAULT_VALUE_SUP));
        return tracker.toDTO();

    }

    @Override
    public TrackerDTO associarTracker(Integer numTracker, Integer numPac) {
        if (validaService.validaAtribuicaoServie(numPac)) {
            Tracker tracker = repo.findTrackerByTrackerId(numTracker).orElseThrow(() -> new NoSuchElementException("Tracker Não Existente"));
            if(tracker.getNumPaciente() != null) throw new IllegalArgumentException("Tracker ja foi associado");
            tracker.setNumPaciente(numPac);
            return repo.save(tracker).toDTO();
        } else {
            throw new IllegalArgumentException("Utente inválido para a associação de tracker");
        }
    }

    @Override
    public List<Long> utentesSemTracker() {
        List<Long> allUtentes = allUtentesAceites();
        List<Long> utentesComTracker = utentesComTracker();
        if(utentesComTracker.size()==0) return allUtentes;
        for (Long id: utentesComTracker) {
            allUtentes.remove(id);
        }
        return allUtentes;
    }

    @Override
    public List<Long> utentesComTracker() {
        List<TrackerDTO> utentesComTracker = listAll();
        List<Long> utentes = allUtentesAceites();
        if(  utentesComTracker==null || utentesComTracker.size()==0) return new ArrayList<>();
        List<Long> idUtentes = new ArrayList<>();
        for (TrackerDTO dto: utentesComTracker) {
            if(dto.numPaciente == null) continue;
            if(utentes.contains(Long.valueOf(dto.numPaciente))) idUtentes.add(Long.valueOf(dto.numPaciente));
        }
        return idUtentes;
    }

    private List<Long> allUtentesAceites(){
        UtenteDTO[] utentes = webClient.get().uri(usersBaseURL + "/api/utente/aceites").retrieve().bodyToMono(UtenteDTO[].class).block();
        List<Long> idUtentes = new ArrayList<>();
        for (UtenteDTO u: utentes) {
            idUtentes.add(u.numPaciente);
        }

        return idUtentes;
    }



    @Override
    public TrackerDTO updateTracker(EditTrackerDTO dto, Integer id) {

        Tracker tracker = repo.findTrackerByTrackerId(id).orElseThrow(() -> new NoSuchElementException("Tracker com id " + id + " não existe"));
        tracker.setLimitesFrequencia(dto.freqCardiacaInferior, dto.freqCardiacaSuperior);
        tracker.setLimitesTemperatura(dto.temperaturaInferior, dto.temperaturaSuperior);
        tracker.setLimitesTensao(dto.tensaoInferior, dto.tensaoSuperior);

        Tracker updatedTracker = repo.save(tracker);
        return updatedTracker.toDTO();

    }


}
