package pt.ipp.isep.labdsoft.HealthTracker.services.rabbitmq.dispatcher;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public final class UtenteExcedeuLimiteEvent implements Serializable {
    private String mensagem;
    private String utenteId;
}