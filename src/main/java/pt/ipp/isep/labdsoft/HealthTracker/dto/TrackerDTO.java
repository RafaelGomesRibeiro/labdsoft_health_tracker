package pt.ipp.isep.labdsoft.HealthTracker.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class TrackerDTO {
    public Integer numPaciente;
    public Integer trackerId;
    public Double tensaoInferior;
    public Double tensaoSuperior;
    public Double temperaturaInferior;
    public Double temperaturaSuperior;
    public Double freqCardiacaInferior;
    public Double freqCardiacaSuperior;
}
