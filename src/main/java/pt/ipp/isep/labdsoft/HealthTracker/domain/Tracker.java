package pt.ipp.isep.labdsoft.HealthTracker.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pt.ipp.isep.labdsoft.HealthTracker.dto.TrackerDTO;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Tracker implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(unique = true)
    private Integer numPaciente;
    @Column(unique = true, nullable = false)
    private Integer trackerId;
    private Double tensaoInferior;
    private Double tensaoSuperior;
    private Double temperaturaInferior;
    private Double temperaturaSuperior;
    private Double freqCardiacaInferior;
    private Double freqCardiacaSuperior;

    public void setNumPaciente(Integer numPaciente) {
        this.numPaciente = numPaciente;
    }

    public Tracker(Integer numPaciente, Integer trackerId, Double tensaoInferior, Double tensaoSuperior, Double temperaturaInferior,
                   Double temperaturaSuperior, Double freqCardiacaInferior, Double freqCardiacaSuperior) {
        if (tensaoInferior != null)
            checkValues(tensaoInferior, tensaoSuperior, freqCardiacaInferior, freqCardiacaSuperior, temperaturaInferior, temperaturaSuperior);
        this.numPaciente = numPaciente;
        this.trackerId = trackerId;
        this.tensaoInferior = tensaoInferior;
        this.tensaoSuperior = tensaoSuperior;
        this.temperaturaInferior = temperaturaInferior;
        this.temperaturaSuperior = temperaturaSuperior;
        this.freqCardiacaInferior = freqCardiacaInferior;
        this.freqCardiacaSuperior = freqCardiacaSuperior;
    }

    private void checkValues(Double tensaoInferior, Double tensaoSuperior, Double freqCardiacaInferior, Double freqCardiacaSuperior, Double temperaturaInferior, Double temperaturaSuperior) {
        if (freqCardiacaInferior >= freqCardiacaSuperior)
            throw new IllegalArgumentException("Limite de frequência cardiaca minimo deve ser menor do que limite máximo");
        if (temperaturaInferior >= temperaturaSuperior)
            throw new IllegalArgumentException("Limite de temperatura minimo deve ser menor do que limite máximo");
        if (tensaoInferior >= tensaoSuperior)
            throw new IllegalArgumentException("Limite de tensao minimo deve ser menor do que limite máximo");
        if (tensaoInferior <= 0) throw new IllegalArgumentException("Tensão deve ser superior a 0");
        if (temperaturaInferior <= 0) throw new IllegalArgumentException("Temperaturas deve ser superior a 0");
        if (freqCardiacaInferior <= 0) throw new IllegalArgumentException("frequência cardiaca deve ser superior a 0");

    }

    public static Tracker fromDTO(TrackerDTO dto) {
        return new Tracker(dto.numPaciente, dto.trackerId, dto.tensaoInferior, dto.tensaoSuperior, dto.temperaturaInferior,
                dto.temperaturaSuperior, dto.freqCardiacaInferior, dto.freqCardiacaSuperior);
    }

    public TrackerDTO toDTO() {
        return new TrackerDTO(this.numPaciente, this.trackerId, this.tensaoInferior, this.tensaoSuperior, this.temperaturaInferior,
                this.temperaturaSuperior, this.freqCardiacaInferior, this.freqCardiacaSuperior);
    }

    public void setLimitesTensao(Double tensaoInf, Double tensaoSup) {
        if (tensaoInf <= 0 || tensaoInf >= tensaoSup) throw new IllegalArgumentException("Tensao Invalida");
        this.tensaoInferior = tensaoInf;
        this.tensaoSuperior = tensaoSup;
    }

    public void setLimitesTemperatura(Double tempInf, Double tempSup) {
        if (tempInf <= 0 || tempInf >= tempSup) throw new IllegalArgumentException("Temperatura Invalida");
        this.temperaturaSuperior = tempSup;
        this.temperaturaInferior = tempInf;
    }

    public void setLimitesFrequencia(Double freqInf, Double freqSup) {
        if (freqInf <= 0 || freqInf >= freqSup) throw new IllegalArgumentException("Frequencia Invalida");

        this.freqCardiacaInferior = freqInf;
        this.freqCardiacaSuperior = freqSup;
    }

    public boolean excedeuLimites(Registo registo) {
        return excedeuLimiteTensao(registo.getTensao()) || excedeuLimiteTemperatura(registo.getTemperatura()) || excedeuFreqCardiaca(registo.getFreqCardiaca());
    }

    private boolean excedeuLimiteTensao(Double tensao) {
        return tensao < this.tensaoInferior || tensao > this.tensaoSuperior;
    }

    private boolean excedeuLimiteTemperatura(Double temperatura) {
        return temperatura < this.temperaturaInferior || temperatura > this.temperaturaSuperior;
    }

    private boolean excedeuFreqCardiaca(Double freqCardiaca) {
        return freqCardiaca < this.freqCardiacaInferior || freqCardiaca > this.freqCardiacaSuperior;
    }

    @Override
    public String toString() {
        return String.format("Limite inferior de tensão: %f | Limite superior de tensão: %f | Limite inferior de temperatura: %f | Limite superior de temperatura: %f | Limite inferior de frequência cardíaca: %f " +
                "| Limite superior de frequência cardíaca: %f", tensaoInferior, tensaoSuperior, temperaturaInferior, temperaturaSuperior, freqCardiacaInferior, freqCardiacaSuperior);
    }
}
