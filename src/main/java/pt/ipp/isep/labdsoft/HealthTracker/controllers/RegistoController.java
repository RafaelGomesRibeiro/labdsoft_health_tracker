package pt.ipp.isep.labdsoft.HealthTracker.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import pt.ipp.isep.labdsoft.HealthTracker.dto.RegistoDTO;
import pt.ipp.isep.labdsoft.HealthTracker.dto.TrackerDTO;
import pt.ipp.isep.labdsoft.HealthTracker.services.RegistoService;
import pt.ipp.isep.labdsoft.HealthTracker.services.TrackerService;


import java.util.List;



@RestController
@RequestMapping("registo")
public final class RegistoController {

    @Autowired
    private RegistoService registoService;

    @GetMapping("/teste")
    public ResponseEntity<String> teste(){
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.TEXT_PLAIN).body("ola");
    }

    @GetMapping("/")
    public ResponseEntity<List<RegistoDTO>> registos(){
        List<RegistoDTO> registos = registoService.listAll();
        return new ResponseEntity<List<RegistoDTO>>(registos,HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<RegistoDTO> byId(@PathVariable Long id) {
        RegistoDTO registo = registoService.byId(id);
        return new ResponseEntity<RegistoDTO>(registo,HttpStatus.OK);
    }

    @GetMapping("/tracker/{numPaciente}")
    public ResponseEntity<List<RegistoDTO>> byId(@PathVariable Integer numPaciente) {
        TrackerDTO dto = new TrackerDTO(numPaciente,0, 0.0,0.0,0.0,0.0,0.0,0.0);
        List<RegistoDTO> registos = registoService.byTracker(dto);
        return new ResponseEntity<List<RegistoDTO>>(registos,HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<RegistoDTO> createTracker(@RequestBody RegistoDTO dto){
        RegistoDTO registo = registoService.createRegisto(dto);
        return new ResponseEntity<RegistoDTO>(registo, HttpStatus.OK);
    }

}
