package pt.ipp.isep.labdsoft.HealthTracker.services;

import pt.ipp.isep.labdsoft.HealthTracker.dto.RegistoDTO;
import pt.ipp.isep.labdsoft.HealthTracker.dto.TrackerDTO;

import java.util.List;


public interface RegistoService {

    List<RegistoDTO> listAll();
    RegistoDTO byId(Long id);
    List<RegistoDTO> byTracker(TrackerDTO dto);
    RegistoDTO createRegisto(RegistoDTO dto);

}
