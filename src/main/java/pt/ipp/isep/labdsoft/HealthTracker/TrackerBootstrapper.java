package pt.ipp.isep.labdsoft.HealthTracker;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import pt.ipp.isep.labdsoft.HealthTracker.controllers.TrackerController;
import pt.ipp.isep.labdsoft.HealthTracker.domain.Tracker;
import pt.ipp.isep.labdsoft.HealthTracker.dto.TrackerDTO;


@Component
public class TrackerBootstrapper implements ApplicationRunner{

    private TrackerController trackerController;

    public TrackerBootstrapper(TrackerController trackerController){
        this.trackerController = trackerController;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        registarTrackers(3);
    }

    private void registarTrackers(int quantidade) {
        for (int i=0; i<quantidade; i++){
            trackerController.createTracker(new TrackerDTO(null, i, null, null, null, null, null, null));
        }
    }
}
