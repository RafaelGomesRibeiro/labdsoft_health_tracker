package pt.ipp.isep.labdsoft.HealthTracker.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EditTrackerDTO {
    public Double tensaoInferior;
    public Double tensaoSuperior;
    public Double temperaturaInferior;
    public Double temperaturaSuperior;
    public Double freqCardiacaInferior;
    public Double freqCardiacaSuperior;
}