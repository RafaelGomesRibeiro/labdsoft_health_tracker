package pt.ipp.isep.labdsoft.HealthTracker.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.labdsoft.HealthTracker.domain.Tracker;

import java.util.Optional;

@Repository
public interface TrackerRepository extends JpaRepository <Tracker, Long> {
    Optional<Tracker> findTrackerByNumPaciente(Integer numPaciente);
    Optional<Tracker> findTrackerByTrackerId(Integer trackerId);
}
