package pt.ipp.isep.labdsoft.HealthTracker.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import pt.ipp.isep.labdsoft.HealthTracker.domain.Registo;
import pt.ipp.isep.labdsoft.HealthTracker.domain.Tracker;
import pt.ipp.isep.labdsoft.HealthTracker.dto.RegistoDTO;
import pt.ipp.isep.labdsoft.HealthTracker.dto.TrackerDTO;
import pt.ipp.isep.labdsoft.HealthTracker.persistence.RegistoRepository;
import pt.ipp.isep.labdsoft.HealthTracker.persistence.TrackerRepository;
import pt.ipp.isep.labdsoft.HealthTracker.services.rabbitmq.dispatcher.EventDispatcher;
import pt.ipp.isep.labdsoft.HealthTracker.services.rabbitmq.dispatcher.TendenciasIdentificadasEvent;
import pt.ipp.isep.labdsoft.HealthTracker.services.rabbitmq.dispatcher.UtenteExcedeuLimiteEvent;
import pt.ipp.isep.labdsoft.HealthTracker.utils.DatesFormatter;


import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@EnableScheduling
@Service
public class RegistoServiceImpl implements RegistoService {

    @Autowired
    private RegistoRepository repo;
    @Autowired
    private TrackerRepository trackerRepository;
    @Autowired
    private EventDispatcher eventDispatcher;

    private final WebClient webClient;
    @Value("${CANDIDATURAS_BASE_URL}"+"/api/avaliacoes/utentesPorProtocolo/")
    private String urlUtentesPorProtocolo;
    private int NUM_REGISTOS_COMPARACAO = 2;
    private double MAX_VARIACAO_TEMPERATURA_ACEITAVEL = 0.1;
    private double MAX_VARIACAO_TENSAO_ACEITAVEL = 0.1;
    private double MAX_VARIACAO_FREQUENCIA_ACEITAVEL = 0.1;
    private double PERCENTAGEM_ALERTA = 0.0000001;


    public RegistoServiceImpl(){
        this.webClient = WebClient.create();
    }

    @Override
    public List<RegistoDTO> listAll() {
        return repo.findAll().stream().map(Registo::toDTO).collect(Collectors.toList());
    }

    @Override
    public RegistoDTO byId(Long id) {
        Registo registo = repo.findById(id).orElseThrow(() -> new NoSuchElementException("Tracker com id " + id + " não existe"));
        return registo.toDTO();
    }

    @Override
    public List<RegistoDTO> byTracker(TrackerDTO dto) {
        Tracker tracker = trackerRepository.findTrackerByNumPaciente(dto.numPaciente).orElseThrow(() -> new NoSuchElementException("Tracker do paciente " + dto.numPaciente + " não existe"));
        List<RegistoDTO> registos = repo.findRegistoByTracker(tracker).stream().map(Registo::toDTO).collect(Collectors.toList());
        return registos;
    }

    @Override
    public RegistoDTO createRegisto(RegistoDTO dto) {
        dto.dataRegisto = DatesFormatter.convertToString(LocalDateTime.now());
        Tracker tracker = trackerRepository.findTrackerByNumPaciente(dto.tracker.numPaciente).orElseThrow(() -> new NoSuchElementException("Tracker do paciente " + dto.tracker.numPaciente + " não existe"));
        Registo registo = Registo.fromDTO(dto, tracker);
        if (tracker.excedeuLimites(registo)) {
            CompletableFuture.runAsync(() -> {
                String mensagem = String.format("Utente com o número de paciente de: %d excedeu os limites configurados para o tracker: %s.\nO registo em causa é o seguinte: %s", tracker.getNumPaciente(), tracker.toString(), registo.toString());
                UtenteExcedeuLimiteEvent e = UtenteExcedeuLimiteEvent.builder().utenteId(String.valueOf(tracker.getNumPaciente()))
                        .mensagem(mensagem).build();
                eventDispatcher.dispatchUtenteExcedeuLimiteEvent(e);
            });
        }
        repo.save(registo);
        return registo.toDTO();
    }

    @Scheduled(fixedDelay = 6000)
    public void alertaTendencias(){
        System.out.println("Iniciou Scheduled Job");
        Map<String, List<Integer>> utentesPorProtocolo = webClient.get().uri(urlUtentesPorProtocolo).retrieve().bodyToMono(HashMap.class).block();
        List<String> alertasPorEmitir = new ArrayList<>();
        System.out.println(utentesPorProtocolo);
        if(utentesPorProtocolo.keySet() == null || utentesPorProtocolo.keySet().isEmpty() ) return;
        for (String protocolo: utentesPorProtocolo.keySet()) {
            List<Integer> utentesProtocolo = utentesPorProtocolo.get(protocolo);
            int numCasosTensao = 0;
            int numCasosTemperatura = 0;
            int numCasosFrequencia = 0;
            for (Integer utente: utentesProtocolo) {
                List<Double> dadosUtente = tratamentoInfoUtente(utente);
                if (dadosUtente==null) continue;
                //Frequencia
                if(dadosUtente.get(0)>MAX_VARIACAO_FREQUENCIA_ACEITAVEL) numCasosFrequencia++;
                //Temperatura
                if(dadosUtente.get(1)>MAX_VARIACAO_TEMPERATURA_ACEITAVEL) numCasosTemperatura++;
                //Tensao
                if(dadosUtente.get(2)>MAX_VARIACAO_TENSAO_ACEITAVEL) numCasosTensao++;
            }
            //Frequencia
            if(utentesProtocolo.size() * PERCENTAGEM_ALERTA <= numCasosFrequencia) alertasPorEmitir.add(protocolo + " - instabilidade Frequência Cardíaca em " + numCasosFrequencia + " utentes!");
            //Temperatura
            if(utentesProtocolo.size() * PERCENTAGEM_ALERTA <= numCasosTemperatura) alertasPorEmitir.add(protocolo + " - instabilidade Temperatura em " + numCasosTemperatura + " utentes!");
            //Tensao
            if(utentesProtocolo.size() * PERCENTAGEM_ALERTA <= numCasosTensao) alertasPorEmitir.add(protocolo + " - instabilidade Tensão Arterial em " + numCasosTensao + " utentes!");
        }
        if(alertasPorEmitir.size()>0){
            System.out.println("Deve enviar e-mail");
            TendenciasIdentificadasEvent e = TendenciasIdentificadasEvent.builder().tendenciasIdentificadas(alertasPorEmitir).build();
            eventDispatcher.dispatchTendenciasIdentificadas(e);
        } else {
            System.out.println("Não deve enviar e-mail");
        }
    }

    private List<Double> tratamentoInfoUtente(Integer utente){
        List<Double> result = new ArrayList<>();

        Tracker trackerUtente = trackerRepository.findTrackerByNumPaciente(utente).orElse(null);
        if(trackerUtente == null) return null;
        List<Registo> registosUtente = repo.findRegistoByTracker(trackerUtente);
        if(registosUtente.size() < 2*NUM_REGISTOS_COMPARACAO) return null;
        List<Registo> registosControlo = registosUtente.subList(0, registosUtente.size()-NUM_REGISTOS_COMPARACAO);
        List<Registo> registosTeste = registosUtente.subList(registosUtente.size()-NUM_REGISTOS_COMPARACAO, registosUtente.size());

        List<Double> somasRegistosControlo = SomaRegistos(registosControlo);
        List<Double> somasRegistosTeste = SomaRegistos(registosTeste);

        Double mediaControloFrequencia = somasRegistosControlo.get(0) / registosControlo.size();
        Double mediaControloTemperatura = somasRegistosControlo.get(1) / registosControlo.size();
        Double mediaControloTensao = somasRegistosControlo.get(2) / registosControlo.size();

        Double mediaTesteFrequencia = somasRegistosTeste.get(0) / NUM_REGISTOS_COMPARACAO;
        Double mediaTesteTemperatura = somasRegistosTeste.get(1) / NUM_REGISTOS_COMPARACAO;
        Double mediaTesteTensao = somasRegistosTeste.get(2) / NUM_REGISTOS_COMPARACAO;

        result.add((mediaTesteFrequencia - mediaControloFrequencia) / mediaControloFrequencia * 100);
        result.add((mediaTesteTemperatura - mediaControloTemperatura) / mediaControloTemperatura * 100);
        result.add((mediaTesteTensao - mediaControloTensao) / mediaControloTensao * 100);

        return result;
    }

    private List<Double> SomaRegistos(List<Registo> registos){
        Double somaTensao = Double.valueOf(0);
        Double somaTemperatura= Double.valueOf(0);
        Double somaFrequencia= Double.valueOf(0);
        for(Registo r: registos){
            somaFrequencia += r.getFreqCardiaca();
            somaTemperatura += r.getTemperatura();
            somaTensao += r.getTensao();
        }

        List<Double> toReturn = Arrays.asList(somaFrequencia, somaTemperatura, somaTensao);
        return toReturn;
    }
}
