package pt.ipp.isep.labdsoft.HealthTracker.services.rabbitmq.dispatcher;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public final class EventDispatcher {
    private static final String TRACKERS_EXCHANGE_NAME = "trackers";
    private static final String UTENTE_EXCEDEU_LIMIT_EVENT_KEY = "A";
    private static final String TENDENCIAS_IDENTIFICADAS_EVENT_KEY = "B";

    private RabbitTemplate rabbitTemplate;

    public EventDispatcher(RabbitTemplate rabbitTemplate){
        this.rabbitTemplate = rabbitTemplate;
    }

    public void dispatchUtenteExcedeuLimiteEvent(UtenteExcedeuLimiteEvent event){
        rabbitTemplate.convertAndSend(TRACKERS_EXCHANGE_NAME, UTENTE_EXCEDEU_LIMIT_EVENT_KEY, event);
    }

    public void dispatchTendenciasIdentificadas(TendenciasIdentificadasEvent event){
        System.out.println("Enviei o evento");
        rabbitTemplate.convertAndSend(TRACKERS_EXCHANGE_NAME, TENDENCIAS_IDENTIFICADAS_EVENT_KEY, event);
    }
}
