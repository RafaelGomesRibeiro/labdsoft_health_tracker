package pt.ipp.isep.labdsoft.HealthTracker.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import pt.ipp.isep.labdsoft.HealthTracker.dto.RegistoDTO;
import pt.ipp.isep.labdsoft.HealthTracker.dto.TrackerDTO;
import pt.ipp.isep.labdsoft.HealthTracker.utils.DatesFormatter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Registo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToOne
    private Tracker tracker;
    private Double tensao;
    private Double temperatura;
    private Double freqCardiaca;
    private LocalDateTime dataRegisto;

    public Registo(Tracker tracker, Double tensao, Double temperatura, Double freqCardiaca, LocalDateTime dataRegisto) {
        this.tracker = tracker;
        this.tensao = tensao;
        this.temperatura = temperatura;
        this.freqCardiaca = freqCardiaca;
        this.dataRegisto = dataRegisto;
    }

    public static Registo fromDTO(RegistoDTO dto, Tracker tracker) {
        return new Registo(tracker, dto.tensao, dto.temperatura, dto.freqCardiaca, DatesFormatter.convertToLocalDateTime(dto.dataRegisto));
    }

    public RegistoDTO toDTO() {
        TrackerDTO tracker = this.tracker.toDTO();
        return new RegistoDTO(tracker, this.tensao, this.temperatura, this.freqCardiaca, DatesFormatter.convertToString(this.dataRegisto));
    }

    @Override
    public String toString() {
        return String.format("Data de registo: %s | Tensão: %f | Temperatura: %f | Frequência Cardíaca: %f", DatesFormatter.convertToString(dataRegisto), tensao, temperatura, freqCardiaca);
    }
}
