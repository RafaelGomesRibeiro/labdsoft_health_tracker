package pt.ipp.isep.labdsoft.HealthTracker.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import pt.ipp.isep.labdsoft.HealthTracker.dto.EditTrackerDTO;
import pt.ipp.isep.labdsoft.HealthTracker.dto.TrackerDTO;
import pt.ipp.isep.labdsoft.HealthTracker.services.TrackerService;


import java.util.List;



@RestController
@RequestMapping("tracker")
public final class TrackerController {

    @Autowired
    private TrackerService trackerService;


    @GetMapping("/teste")
    public ResponseEntity<String> teste(){
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.TEXT_PLAIN).body("ola");
    }

    @GetMapping("/")
    public ResponseEntity<List<TrackerDTO>> enfermeiros(){
        List<TrackerDTO> trackers = trackerService.listAll();
        return new ResponseEntity<List<TrackerDTO>>(trackers,HttpStatus.OK);
    }

    @GetMapping("/utentesSemTracker/")
    public ResponseEntity<List<Long>> utentesSemTracker(){
        return new ResponseEntity<List<Long>>(trackerService.utentesSemTracker(), HttpStatus.OK);
    }

    @GetMapping("/utentesComTracker/")
    public ResponseEntity<List<Long>> utentesComTracker(){
        return new ResponseEntity<List<Long>>(trackerService.utentesComTracker(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TrackerDTO> byId(@PathVariable Integer id) {
        TrackerDTO tracker = trackerService.byId(id);
        return new ResponseEntity<TrackerDTO>(tracker,HttpStatus.OK);
    }

    @GetMapping("/utente/{numPaciente}")
    public ResponseEntity<TrackerDTO> byPaciente(@PathVariable Integer numPaciente) {
        TrackerDTO tracker = trackerService.byNumPaciente(numPaciente);
        return new ResponseEntity<TrackerDTO>(tracker,HttpStatus.OK);
    }

    @PutMapping("/associar/{numTracker}/{numPaciente}")
    public ResponseEntity<TrackerDTO> associarTracker(@PathVariable Integer numTracker,@PathVariable Integer numPaciente){
        TrackerDTO tracker = trackerService.associarTracker(numTracker,numPaciente);
        return new ResponseEntity<TrackerDTO>(tracker, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<TrackerDTO> createTracker(@RequestBody TrackerDTO dto){
        TrackerDTO tracker = trackerService.createTracker(dto);
        return new ResponseEntity<TrackerDTO>(tracker, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TrackerDTO> updateTracker(@RequestBody EditTrackerDTO dto, @PathVariable Integer id){
        TrackerDTO tracker = trackerService.updateTracker(dto, id);
        return new ResponseEntity<TrackerDTO>(tracker, HttpStatus.OK);
    }
}
