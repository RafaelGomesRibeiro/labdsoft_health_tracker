package pt.ipp.isep.labdsoft.HealthTracker.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class TrackerTest {



    @Test
    public void createInvalidTensao(){
        try {

            Tracker r = new Tracker(1, 2, 12.0, 2.0, 1.0, 2.0, 1.0, 2.0);
        }catch (Exception e){
            Assertions.assertEquals(e.getMessage(),"Limite de tensao minimo deve ser menor do que limite máximo");

        }
    }

    @Test
    public void createInvalidTemperatura(){
        try {

            Tracker r = new Tracker(1, 2, 1.0, 2.0, 12.0, 2.0, 1.0, 2.0);
        }catch (Exception e){
            Assertions.assertEquals("Limite de temperatura minimo deve ser menor do que limite máximo",e.getMessage());

        }
    }

    @Test
    public void createInvalidFreq(){
        try {
            Tracker r = new Tracker(1,2,1.0,2.0,1.0,2.0,12.0,2.0);

         }catch (Exception e){
            Assertions.assertEquals("Limite de frequência cardiaca minimo deve ser menor do que limite máximo",e.getMessage());

    }
    }


    @Test
    public void createNegTensao(){
        try {
            Tracker r = new Tracker(1,2,-1.0,2.0,1.0,2.0,1.0,2.0);

        }catch (Exception e){
            Assertions.assertEquals("Tensão deve ser superior a 0",e.getMessage());

        }
    }

    @Test
    public void createNegativeTemp(){
        try {
            Tracker r = new Tracker(1,2,1.0,2.0,-1.0,2.0,1.0,2.0);

        }catch (Exception e){
            Assertions.assertEquals("Temperaturas deve ser superior a 0",e.getMessage());

        }
    }

    @Test
    public void createNegFreq(){
        try {
            Tracker r = new Tracker(1,2,1.0,2.0,1.0,2.0,-1.0,2.0);

        }catch (Exception e){
            Assertions.assertEquals("frequência cardiaca deve ser superior a 0",e.getMessage());

        }
    }

}
